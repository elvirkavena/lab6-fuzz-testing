import {calculateBonuses} from "./bonus-system.js";
const assert = require("assert");


describe('Bonus system tests', () => {
    let app;
    console.log("Tests started");
    
    test('Standard1',  (done) => {
      let a = "Standard";
      let b = 10000;
      expect(calculateBonuses(a, b)).toEqual(0.05*1.5);
      done();
    });
    test('Standard2',  (done) => {
      let a = "Standard";
      let b = 50000;
      expect(calculateBonuses(a, b)).toEqual(0.05*2);
      done();
    });
    
    test('Standard3',  (done) => {
      let a = "Standard";
      let b = 100000;
      expect(calculateBonuses(a, b)).toEqual(0.05*2.5);
      done();
    });

    test('minus Testtttt',  (done) => {
      let a = "minus";
      let b = -1000;
      expect(calculateBonuses(a, b)).toEqual(0);
      done();
    });
    
    test('Standard1',  (done) => {
      let a = "Standard";
      let b = 5000;
      expect(calculateBonuses(a, b)).toEqual(0.05 * 1);
      done();
    });
    
    test('Standard2',  (done) => {
      let a = "Standard";
      let b = 45000;
      expect(calculateBonuses(a, b)).toEqual(0.05*1.5);
      done();
    });
    
    test('Standard3',  (done) => {
      let a = "Standard";
      let b = 95000;
      expect(calculateBonuses(a, b)).toEqual(0.05*2);
      done();
    });
    
    test('Standard4',  (done) => {
      let a = "Standard";
      let b = 105000;
      expect(calculateBonuses(a, b)).toEqual(0.05*2.5);
      done();
    });
    
    test('Premium1',  (done) => {
      let a = "Premium";
      let b = 5000;
      expect(calculateBonuses(a, b)).toEqual(0.1 * 1);
      done();
    });
    
    test('Premium2',  (done) => {
      let a = "Premium";
      let b = 45000;
      expect(calculateBonuses(a, b)).toEqual(0.1 * 1.5);
      done();
    });
    
    test('Premium3',  (done) => {
      let a = "Premium";
      let b = 95000;
      expect(calculateBonuses(a, b)).toEqual(0.1 *2);
      done();
    });
    
    test('Premium4',  (done) => {
      let a = "Premium";
      let b = 105000;
      expect(calculateBonuses(a, b)).toEqual(0.1 * 2.5);
      done();
    });
    
    test('Diamond1',  (done) => {
      let a = "Diamond";
      let b = 5000;
      expect(calculateBonuses(a, b)).toEqual(0.2 * 1);
      done();
    });
    
    test('Diamond2',  (done) => {
      let a = "Diamond";
      let b = 45000;
      expect(calculateBonuses(a, b)).toEqual(0.2 * 1.5);
      done();
    });
    
    test('Diamond3',  (done) => {
      let a = "Diamond";
      let b = 95000;
      expect(calculateBonuses(a, b)).toEqual(0.2 *2);
      done();
    });
    
    test('Diamond4',  (done) => {
      let a = "Diamond";
      let b = 105000;
      expect(calculateBonuses(a, b)).toEqual(0.2 * 2.5);
      done();
    });
    
    console.log('Testing is Finished');

});

